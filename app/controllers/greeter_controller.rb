class GreeterController < ApplicationController
  def hello
    get_name_and_time
    @times_displayed ||= 0  # Este operador ( ||= ) pondrá el valor de la derecha a una nueva variable de la izquierda o dejará el valor de la variable de la izquierda si esta ya existía
    @times_displayed += 1
  end

  def goodbye
    get_name_and_time
  end

  private
  
  def get_name_and_time
    random_names = ["Verónica", "Julio", "Peluchosa"]
    @name = random_names.sample
    @time = Time.now
  end
  
end

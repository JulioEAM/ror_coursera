Rails.application.routes.draw do
  get 'greeter/hello'
  get 'greeter/goodbye'
  get '/', to: 'greeter#hello'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
